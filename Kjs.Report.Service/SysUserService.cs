﻿using Kjs.Report.Domain.Table;
using Kjs.Report.Infrastructure;
using Kjs.Report.IService;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using System.Linq.Expressions;

namespace Kjs.Report.Service
{
    public class SysUserService : ISysUserService
    {
        private readonly IUnitOfWork _unitOfWork;
        private IRepository<D_Sys_User> _repository;

        public SysUserService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _repository = _unitOfWork.GetRepository<D_Sys_User>();
        }

        #region 公共方法

        public async Task<D_Sys_User> Get(string userName)
        {
            return await _repository.GetFirstOrDefaultAsync(x => x.user_name == userName, q => q.OrderBy(d => d.id));
        }

        public async Task<List<D_Sys_User>> GetUserList()
        {
            var page = await _repository.GetPagedListAsync(pageSize: Const.maxPageSize);
            return page.Items.ToList();
        }

        public async Task<int> Add(D_Sys_User user)
        {
            await _repository.InsertAsync(user);
            return await _unitOfWork.SaveChangesAsync();
        }

        public async Task<int> Update(D_Sys_User user)
        {
            _repository.Update(user);
            return await _unitOfWork.SaveChangesAsync();
        }

        public int Delete(int userId)
        {
            int count = _unitOfWork.ExecuteSqlCommand("delete from  sys_user where id={0}", userId);
            return count;
        }

        public int UpdateUserRole(int roleId, int userId)
        {
            int count = _unitOfWork.ExecuteSqlCommand("update sys_user set role_id={0} where id={1}", roleId, userId);
            return count;
        }

        public int UpdateUserPwd(int userId, string newPwd)
        {
            int count = _unitOfWork.ExecuteSqlCommand("update sys_user set password={0} where id={1}", newPwd, userId);
            return count;
        }

        public bool CheckUserPwd(int userId, string oldPwd)
        {
            int count = _repository.Count(predicate: x => x.id == userId && x.password == oldPwd);
            return count > 0;
        }

        public async Task<List<int>> GetUserIdsByRoleId(int roleId)
        {
            var page = await _repository.GetPagedListAsync(predicate: x => x.role_id == roleId, pageSize: Const.maxPageSize);
            var list = page.Items.Select(x => x.id).ToList();
            return list;
        }

        public bool ExistsNameOrNo(string userName, string userNo, int userId)
        {
            Expression<Func<D_Sys_User, bool>> predicate = x => x.user_name == userName || x.user_no == userNo;
            if (userId > 0)
            {
                predicate = predicate.And(x => x.id != userId);
            }

            var count = _repository.Count(predicate: predicate);
            return count > 0;
        }

        #endregion
    }
}
