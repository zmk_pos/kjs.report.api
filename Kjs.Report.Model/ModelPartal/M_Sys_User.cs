﻿namespace Kjs.Report.Model
{
    /// <summary>
    /// 系统用户类
    /// </summary>
    public partial class M_Sys_User
    {
        /// <summary>
        /// 角色类型
        /// </summary>
        public string roleType { get; set; }
    }
}
