﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kjs.Report.Model
{
    public class PagedList<T>
    {
        public int totalCount { get; set; }

        public T items { get; set; }
    }
}
