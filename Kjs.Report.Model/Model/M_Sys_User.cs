﻿using System;

namespace Kjs.Report.Model
{
    /// <summary>
    /// 系统用户类
    /// </summary>
    public partial class M_Sys_User
    {
        /// <summary>
        /// 用户id
        /// </summary>
        public int userId { get; set; }
        /// <summary>
        /// 用户编号
        /// </summary>
        public string userNo { get; set; }
        /// <summary>
        /// 用户名称
        /// </summary>
        public string userName { get; set; }
        /// <summary>
        /// 头像
        /// </summary>
        public string headImg { get; set; }
        /// <summary>
        /// 密码
        /// </summary>
        public string password { get; set; }
        /// <summary>
        /// 真实姓名
        /// </summary>
        public string realName { get; set; }
        /// <summary>
        /// 所属角色
        /// </summary>
        public int? roleId { get; set; }
        /// <summary>
        /// 邮箱
        /// </summary>
        public string email { get; set; }
        /// <summary>
        /// 手机
        /// </summary>
        public string phone { get; set; }
        /// <summary>
        /// 是否管理员
        /// </summary>
        public bool? isSuper { get; set; }
        /// <summary>
        /// 是否启用
        /// </summary>
        public bool? isEnable { get; set; }
        /// <summary>
        /// 添加人
        /// </summary>
        public int? addUser { get; set; }
        /// <summary>
        /// 添加时间
        /// </summary>
        public DateTime? addTime { get; set; }
        /// <summary>
        /// 修改人
        /// </summary>
        public int? updateUser { get; set; }
        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime? updateTime { get; set; }
        /// <summary>
        /// 最后登录IP
        /// </summary>
        public string lastLoginIp { get; set; }
        /// <summary>
        /// 最后登录时间
        /// </summary>
        public DateTime? lastLoginTime { get; set; }
    }
}
