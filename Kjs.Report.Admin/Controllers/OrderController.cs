﻿using Kjs.Report.IService;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;

namespace Kjs.Report.Admin.Controllers
{
    /// <summary>
    /// 订单统计
    /// </summary>
    [Produces("application/json")]
    [Authorize("Bearer")]
    [EnableCors("Any")]
    [Route("api/order")]
    public class OrderController : ApiControllerBase
    {
        private readonly IOrderService _orderService;
        /// <summary>
        /// 构造方法
        /// </summary>
        /// <param name="orderReportDetailService"></param>
        public OrderController(IOrderService orderReportDetailService)
        {
            _orderService = orderReportDetailService;
        }

        /// <summary>
        /// 按照终端编号分组统计获取数据明细
        /// </summary>
        /// <param name="orgIds">组织编号,多个用 , 隔开[英文]</param>
        /// <param name="pageSize">每页数量</param>
        /// <param name="pageIndex">当前页码</param>
        /// <param name="line_id">线路编号</param>
        /// <param name="point_id">点位编号</param>
        /// <param name="beginTime">开始时间</param>
        /// <param name="endTime">结束时间</param>
        /// <returns></returns>
        [HttpGet("sale/terminalgroup/page/list")]
        public object TerminalGroupList(string orgIds = "", int pageSize = 20, int pageIndex = 1, int line_id = 0, int point_id = 0, DateTime? beginTime = null,
            DateTime? endTime = null)
        {
            return _orderService.TerminalGroupList(pageSize, pageIndex, "", orgIds, line_id, point_id, beginTime, endTime);
        }


        /// <summary>
        /// 按照商品名称分组统计获取数据明细
        /// </summary>
        /// <param name="orgIds">组织编号,多个用 , 隔开[英文]</param>
        /// <param name="pageSize">每页数量</param>
        /// <param name="pageIndex">当前页码</param>
        /// <param name="line_id">线路编号</param>
        /// <param name="point_id">点位编号</param>
        /// <param name="beginTime">开始时间</param>
        /// <param name="endTime">结束时间</param>
        /// <returns></returns>
        [HttpGet("sale/goodgroup/page/list")]
        public object GoodTitleGroupList(string orgIds = "", int pageSize = 20, int pageIndex = 1, int line_id = 0, int point_id = 0, DateTime? beginTime = null,
            DateTime? endTime = null)
        {
            return _orderService.GoodTitleGroupList(pageSize, pageIndex, "", orgIds, line_id, point_id, beginTime, endTime);
        }



        /// <summary>
        /// 按照商品分类分组统计获取数据明细
        /// </summary>
        /// <param name="orgIds">组织编号,多个用 , 隔开[英文]</param>
        /// <param name="pageSize">每页数量</param>
        /// <param name="pageIndex">当前页码</param>
        /// <param name="line_id">线路编号</param>
        /// <param name="point_id">点位编号</param>
        /// <param name="beginTime">开始时间</param>
        /// <param name="endTime">结束时间</param>
        /// <returns></returns>
        [HttpGet("sale/categorygroup/page/list")]
        public object CateogryGroupList(string orgIds = "", int pageSize = 20, int pageIndex = 1, int line_id = 0, int point_id = 0, DateTime? beginTime = null,
            DateTime? endTime = null)
        {
            return _orderService.CategoryGroupList(pageSize, pageIndex, "", orgIds, line_id, point_id, beginTime, endTime);
        }

        /// <summary>
        /// 按照日期分组统计获取数据明细
        /// </summary>
        /// <param name="pageSize">每页数量</param>
        /// <param name="orgIds">组织编号,多个用 , 隔开[英文]</param>
        /// <param name="pageIndex">当前页码</param>
        /// <param name="line_id">线路编号</param>
        /// <param name="point_id">点位编号</param>
        /// <param name="beginTime">开始时间</param>
        /// <param name="endTime">结束时间</param>
        /// <returns></returns>
        [HttpGet("sale/timegroup/page/list")]
        public object TimeGroupList(string orgIds = "", int pageSize = 20, int pageIndex = 1, int line_id = 0, int point_id = 0, DateTime? beginTime = null,
            DateTime? endTime = null)
        {
            return _orderService.TimeGroupList(pageSize, pageIndex, "", orgIds, line_id, point_id, beginTime, endTime);
        }

        /// <summary>
        /// 按照日期及订单状态分组统计获取数据明细
        /// </summary>
        /// <param name="orgIds">组织编号,多个用 , 隔开[英文]</param>
        /// <param name="pageSize">每页数量</param>
        /// <param name="pageIndex">当前页码</param>
        /// <param name="line_id">线路编号</param>
        /// <param name="point_id">点位编号</param>
        /// <param name="beginTime">开始时间</param>
        /// <param name="endTime">结束时间</param>
        /// <returns></returns>
        [HttpGet("sale/timestatusgroup/page/list")]
        public object TimeStatusGroupList(string orgIds = "", int pageSize = 20, int pageIndex = 1, int line_id = 0, int point_id = 0, DateTime? beginTime = null,
            DateTime? endTime = null)
        {
            return _orderService.TimeStatusGroupList(pageSize, pageIndex, "", orgIds, line_id, point_id, beginTime, endTime);
        }

        /// <summary>
        /// 按照本机编号及订单状态分组统计获取数据明细
        /// </summary>
        /// <param name="orgIds">组织编号,多个用 , 隔开[英文]</param>
        /// <param name="pageSize">每页数量</param>
        /// <param name="pageIndex">当前页码</param>
        /// <param name="line_id">线路编号</param>
        /// <param name="point_id">点位编号</param>
        /// <param name="beginTime">开始时间</param>
        /// <param name="endTime">结束时间</param>
        /// <returns></returns>
        [HttpGet("sale/terminalstatusgroup/page/list")]
        public object TerminalStatusGroupList(string orgIds = "", int pageSize = 20, int pageIndex = 1, int line_id = 0, int point_id = 0, DateTime? beginTime = null,
            DateTime? endTime = null)
        {
            return _orderService.TerminalStatusGroupList(pageSize, pageIndex, "", orgIds, line_id, point_id, beginTime, endTime);
        }
    }
}