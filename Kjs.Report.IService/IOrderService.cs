﻿using Kjs.Report.Model;
using System;
using System.Collections.Generic;

namespace Kjs.Report.IService
{
    /// <summary>
    /// 订单报表
    /// </summary>
    public interface IOrderService
    {
        /// <summary>
        /// 按照设备分组统计销售明细
        /// </summary>
        /// <param name="pageSize">每页数量</param>
        /// <param name="pageIndex">当前页码</param>
        /// <param name="filedOrder">排序字段</param>
        /// <param name="orgIds">组织编号[多个用  , 隔开]</param>
        /// <param name="line_id">线路编号</param>
        /// <param name="point_id">点位编号</param>
        /// <param name="beginTime">开始时间</param>
        /// <param name="endTime">结束时间</param>
        /// <param name="IsSuper">是否为超级角色</param>
        /// <returns></returns>
        PagedList<IList<M_TerminalGroup>> TerminalGroupList(int pageSize, int pageIndex, string filedOrder, string orgIds, int line_id, int point_id,
            DateTime? beginTime, DateTime? endTime);

        /// <summary>
        /// 按照商品分组统计销售明细
        /// </summary>
        /// <param name="pageSize">每页数量</param>
        /// <param name="pageIndex">当前页码</param>
        /// <param name="filedOrder">排序字段</param>
        /// <param name="orgIds">组织编号[多个用  , 隔开]</param>
        /// <param name="line_id">线路编号</param>
        /// <param name="point_id">点位编号</param>
        /// <param name="beginTime">开始时间</param>
        /// <param name="endTime">结束时间</param>
        /// <returns></returns>
        PagedList<IList<M_GoodTitleGroup>> GoodTitleGroupList(int pageSize, int pageIndex, string filedOrder, string orgIds, int line_id, int point_id,
                    DateTime? beginTime, DateTime? endTime);
        /// <summary>
        ///  按照商品分类分组统计销售明细
        /// </summary>
        /// <param name="pageSize">每页数量</param>
        /// <param name="pageIndex">当前页码</param>
        /// <param name="orgIds">组织编号[多个用  , 隔开]</param>
        /// <param name="line_id">线路编号</param>
        /// <param name="point_id">点位编号</param>
        /// <param name="beginTime">开始时间</param>
        /// <param name="endTime">结束时间</param>
        /// <returns></returns>
        PagedList<IList<M_CategoryGroup>> CategoryGroupList(int pageSize, int pageIndex, string filedOrder, string orgIds, int line_id, int point_id,
            DateTime? beginTime, DateTime? endTime);

        /// <summary>
        ///  按照日期分组统计销售明细
        /// </summary>
        /// <param name="pageSize">每页数量</param>
        /// <param name="pageIndex">当前页码</param>
        /// <param name="orgIds">组织编号[多个用  , 隔开]</param>
        /// <param name="line_id">线路编号</param>
        /// <param name="point_id">点位编号</param>
        /// <param name="beginTime">开始时间</param>
        /// <param name="endTime">结束时间</param>
        /// <returns></returns>
        PagedList<IList<M_TimeGroup>> TimeGroupList(int pageSize, int pageIndex, string filedOrder, string orgIds, int line_id, int point_id,
               DateTime? beginTime, DateTime? endTime);

        /// <summary>
        ///  按照日期及订单状态分组统计
        /// </summary>
        /// <param name="pageSize">每页数量</param>
        /// <param name="pageIndex">当前页码</param>
        /// <param name="orgIds">组织编号[多个用  , 隔开]</param>
        /// <param name="line_id">线路编号</param>
        /// <param name="point_id">点位编号</param>
        /// <param name="beginTime">开始时间</param>
        /// <param name="endTime">结束时间</param>
        /// <returns></returns>
        PagedList<IList<M_TimeStatusGroup>> TimeStatusGroupList(int pageSize, int pageIndex, string filedOrder, string orgIds, int line_id, int point_id,
    DateTime? beginTime, DateTime? endTime);

        /// <summary>
        ///  按照日期分组统计销售明细
        /// </summary>
        /// <param name="orgIds">组织编号[多个用  , 隔开]</param>
        /// <param name="line_id">线路编号</param>
        /// <param name="point_id">点位编号</param>
        /// <param name="beginTime">开始时间</param>
        /// <param name="endTime">结束时间</param>
        /// <returns></returns>
        PagedList<IList<M_TerminalStatusGroup>> TerminalStatusGroupList(int pageSize, int pageIndex, string filedOrder, string orgIds, int line_id, int point_id,
            DateTime? beginTime, DateTime? endTime);

    }
}
