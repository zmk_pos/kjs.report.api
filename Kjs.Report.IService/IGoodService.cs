﻿using Kjs.Report.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Kjs.Report.IService
{
    /// <summary>
    /// 商品报表
    /// </summary>
    public interface IGoodService
    {

        /// <summary>
        /// 按照商品分组统计在售明细
        /// </summary>
        /// <param name="pageSize">每页数量</param>
        /// <param name="pageIndex">当前页码</param>
        /// <param name="orgIds">组织编号[多个用  , 隔开]</param>
        /// <param name="line_id">线路编号</param>
        /// <param name="point_id">点位编号</param>
        /// <returns></returns>
        PagedList<IList<M_GoodSaleGroup>> GoodSaleGroupList(int pageSize, int pageIndex, string filedOrder, string orgIds, int line_id, int point_id);
    }
}
