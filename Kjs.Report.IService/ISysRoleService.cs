﻿using Kjs.Report.Domain.Table;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Kjs.Report.IService
{
    public interface ISysRoleService
    {
        Task<D_Sys_Role> Get(int id);

        Task<List<D_Sys_Role>> GetList();

        Task<Tuple<IList<D_Sys_Role>, int>> GetPageList(string keyWord, int pageIndex, int pageSize);

        Task<int> Add(D_Sys_Role sysRole);

        Task<int> Update(D_Sys_Role sysRole);

        Task<int> Delete(D_Sys_Role sysRole);
    }
}
